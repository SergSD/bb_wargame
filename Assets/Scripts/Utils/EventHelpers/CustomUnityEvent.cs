﻿using UnityEngine.Events;

[System.Serializable]
public class CustomUnityEvent<T> : UnityEvent<T>
{
}
