﻿using System;
using System.Collections;
using UnityEngine;

namespace BB_WarGame
{
    public class FadeInOutCard : MonoBehaviour
    {
        private bool _isFadeOut = false;
        private Action _callback = null;
        public void FadeCardOut(bool isFadeOut, float duration, Action callback = null)
        {
            _isFadeOut = isFadeOut;
            _callback = callback;

            var src = _isFadeOut ? 1f : .7f;
            var dest = _isFadeOut ? .7f : 1f;
            Hashtable param = new Hashtable();
            param.Add("from", src);
            param.Add("to", dest);
            param.Add("time", duration);
            param.Add("onupdate", "TweenedValue");
            param.Add("oncomplete", "TweenedComplete");
            iTween.ValueTo(gameObject, param);
            TweenedValue(src);
        }
        private void TweenedValue(float val)
        {
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, val);
        }

        private void TweenedComplete()
        {
            StartCoroutine(TweenedComplete2());
        }
        IEnumerator TweenedComplete2()
        {
            yield return new WaitForSeconds(1.5f);
            _isFadeOut = false;
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            if (_callback != null)
            {
                var call = _callback;
                _callback = null;
                call();
            }
        }
    }

}
