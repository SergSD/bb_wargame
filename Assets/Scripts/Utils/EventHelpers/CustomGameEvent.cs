﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BB_WarGame
{
    public class CustomGameEvent : CustomEnumeration
    {
        public static readonly string GAME_FINISHED = new CustomGameEvent("GAME_FINISHED", "GAME_FINISHED").ToString();
        public static readonly string START_NEW_GAME =  new CustomGameEvent("START_NEW_GAME", "START_NEW_GAME").ToString();
        public  CustomGameEvent(string name, string value) : base(name, value) { }
    }
}