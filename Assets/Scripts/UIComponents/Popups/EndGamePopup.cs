﻿using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace BB_WarGame
{
    public class EndGamePopup : MonoBehaviour
    {
        [SerializeField] private Text _text;
        [SerializeField] private Button _newGameBtn;
        private Action _onCloseCallback;

        public void Hide()
        {
            gameObject.SetActive(false);
        }
        public void Show(bool isDraw, int winningPlayerIndex, Action callback)
        {
            _onCloseCallback = callback;
            gameObject.SetActive(true);

            string msg;
            if (isDraw)
            {
                msg = "Draw!";
            }
            else
            {
                msg = string.Format("The Winner is Player{0} ({1})", winningPlayerIndex, winningPlayerIndex == 1 ? "Human" : "PC");
            }

            _text.text = msg;
        }

        public void OnCloseBtnCLikced()
        {
            _newGameBtn.interactable = false;
            Hide();
            _onCloseCallback?.Invoke();
        }

        void Start()
        {
            _newGameBtn.interactable = true;            
        }
    }
}