﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace BB_WarGame
{

    public abstract class CustomEnumeration
    {
        public string Name { get; private set; }

        public string Value { get; private set; }

        protected CustomEnumeration(string name, string value)
        {
            Value = value;
            Name = name;
        }

        public override string ToString() => Value;

        public static IEnumerable<T> GetAll<T>() where T : CustomEnumeration
        {
            var fields = typeof(T).GetFields(BindingFlags.Public |
                                             BindingFlags.Static |
                                             BindingFlags.DeclaredOnly);

            return fields.Select(f => f.GetValue(null)).Cast<T>();
        }
    }
}