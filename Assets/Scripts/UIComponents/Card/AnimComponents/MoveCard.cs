﻿using UnityEngine;
using System.Collections;
using System;
using ExtraUtils;

namespace BB_WarGame
{
    public class MoveCard : MonoBehaviour
    {

        private Vector2 _startPos;
        private Vector2 _targetPos;
        private Vector2? _curvOffet;
        private Action _onCompleteCallback;

        void Start()
        {
            //gameObject.SetActive(false);
        }

        public void AnimateMoveToDist(Vector2 target, Vector2? curvOffset , Action callback = null)
        {
           _onCompleteCallback = callback;
            _targetPos = target;
            _startPos = transform.localPosition;
            _curvOffet  = curvOffset;

            Hashtable param = new Hashtable();
            param.Add("easetype", "easeOutQuad");
            param.Add("from", 0.0f);
            param.Add("to", 1.0f);
            param.Add("time", 1.0f);
            param.Add("onupdate", "TweenedValue");
            param.Add("oncomplete", "TweenedComplete1");
            iTween.ValueTo(gameObject, param);
            TweenedValue(0);
        }
        private void TweenedValue(float val)
        {
            if (_curvOffet != null)
                transform.localPosition = TweenUtils.GetQuadraticBezierCurvePosition(val, _startPos, _targetPos, (Vector2)_curvOffet);
            else
                transform.localPosition = TweenUtils.GetLinearBezierCurvePosition(val, _startPos, _targetPos);
        }

        private void TweenedComplete1()
        { 
          StartCoroutine(TweenedComplete2());
        }

        IEnumerator TweenedComplete2()
        {
            yield return new WaitForSeconds(0.2f);
            _onCompleteCallback?.Invoke();
        }
    }
   
}
