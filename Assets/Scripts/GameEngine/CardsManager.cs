﻿using UnityEngine;
using System.Collections;
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using ExtraUtils;
using UnityEngine.Rendering;

namespace BB_WarGame
{
    public class CardsManager : MonoBehaviour
    {                
        [SerializeField] private CardsComponent _cardsComponent;
        private List<int> _cards1;
        private List<int> _cards2;
        private List<int> _pendingCards = new List<int>();
        private int _currentPlayerCardId;
        private int _currentOpponentCardId;
        private bool _isWar;
        private const int NUM_OF_SUITS = 4;
        private const int NUM_OF_CARDS_IN_DECK = 52;

        // Use this for initialization
        void Start()
        {
            
            SetupNewGame();
        }

        public void SetupNewGame()
        {
            GenerateCardsLists();
            UpdateCardsCoount();
            _cardsComponent.SetButtonsActive(true);
        }

        public void StartTurn()
        {
            int playerCardValue, opponentCardValue;            
            _cardsComponent.SetButtonsActive(false);
            _currentPlayerCardId = PullCardFromTop(1);
            _currentOpponentCardId = PullCardFromTop(2);
            UpdateCardsCoount();
            playerCardValue = GetCardValue(_currentPlayerCardId);
            opponentCardValue = GetCardValue(_currentOpponentCardId);
            if ((playerCardValue != opponentCardValue) && !_isWar)
            {
                _isWar = false;
                int winnerId = playerCardValue > opponentCardValue ? 1 : 2;                
                _pendingCards.AddRange(new List<int>() { _currentPlayerCardId, _currentOpponentCardId });
                AddCardsAtTheEnd(winnerId, _pendingCards);
                PlayRegularTurn(winnerId);
            }
            else
            {                
                _pendingCards.AddRange(new List<int>() { _currentPlayerCardId, _currentOpponentCardId });
                PlayWarTurn();
            }
        }
        public void UpdateCardsCoount()
        {
            _cardsComponent.SetPlayerCardsLeft(_cards1.Count);
            _cardsComponent.SetOpponentCardsLeft(_cards2.Count);
        }
        public void PlayRegularTurn(int winnerId)
        {
            _cardsComponent.PlayRegularTurn( _currentPlayerCardId, _currentOpponentCardId, winnerId, OnTurnCompleted);
        }

        public void PlayWarTurn()
        {
            if (!_isWar)
            {
                _isWar = true;
                _cardsComponent.PlayWarTurn(_currentPlayerCardId, _currentOpponentCardId, OnTurnCompleted);
            }
            else
            {
                _cardsComponent.PlayWarTurn(OnWarTurnCompleted);
            }
        }

        private void OnWarTurnCompleted(int numOfCards)
        {
            if (CheckAndHandleGameFinished()) return;
            _isWar = false;
            StartTurn();
        }

        private void OnTurnCompleted(int numOfCards)
        {
            if (CheckAndHandleGameFinished()) return;
            if (_isWar)
            {
                StartTurn();
                return;
            }
            _cardsComponent.SetButtonsActive(true);
            
            UpdateCardsCoount();
            
        }

        private bool CheckAndHandleGameFinished()
        {
            if (_cards1.Count == 0 || _cards2.Count == 0)
            {
                _pendingCards.Clear();
                if (_cards1.Count == _cards2.Count)
                {
                    EventManager.TriggerEvent(CustomGameEvent.GAME_FINISHED, 0);
                    return true;
                }
                EventManager.TriggerEvent(CustomGameEvent.GAME_FINISHED, _cards1.Count > 0 ? 1 : 2);
                return true;
            }
            return false;
        }

        private void GenerateCardsLists()
        {
            int[] fullDeck = new int[NUM_OF_CARDS_IN_DECK];

            _cards1?.Clear();
            _cards2?.Clear();

            _cards1 = new List<int>();
            _cards2 = new List<int>();
            
            for (int i = 0; i < NUM_OF_CARDS_IN_DECK; i++)
            {
                fullDeck[i] = i;
            }
            Utils.Shuffle(fullDeck);
            for (int j = 0; j < NUM_OF_CARDS_IN_DECK / 2; j++)
            {
                _cards1.Add(fullDeck[j]);
                _cards2.Add(fullDeck[j + NUM_OF_CARDS_IN_DECK / 2]);
            }
        }

        private int PullCardFromTop(int playerIndex)
        {
            List<int> prop = playerIndex == 1 ? _cards1 : _cards2;
            int retIndex = prop.FirstOrDefault();
            prop.RemoveAt(0);
            return retIndex;
        }

        private void AddCardsAtTheEnd(int playerIndex, List<int> cardsInd)
        {
            List<int> prop = playerIndex == 1 ? _cards1 : _cards2;
            prop.AddRange(cardsInd);
        }

        private int GetCardValue(int cardIdx)
        {
            return cardIdx % (NUM_OF_CARDS_IN_DECK / NUM_OF_SUITS);
        }

        public void SetSuddenDeath()
        {
            if (!_isWar)
            {
                _cards1 = new List<int>() { _cards1.First() };
                _cards2 = new List<int>() { _cards2.First() };
                UpdateCardsCoount();
            }
        }
        
    }
}