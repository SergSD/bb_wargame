﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BB_WarGame
{
    public class ScaleInOutCard : MonoBehaviour
    {
        private Action _callback = null;
        public void ScaleInOut(float duration, Action callback = null)
        {
            _callback = callback;
            Hashtable param = new Hashtable();
            param.Add("x", 1.2f);
            param.Add("y", 1.2f);
            param.Add("z", 1.0f);
            param.Add("time", duration);
            param.Add("looptype", iTween.LoopType.none);
            param.Add("oncomplete", "TweenedComplete");
            iTween.ScaleTo(gameObject, param);
        }

        private void TweenedComplete()
        {
            StartCoroutine(TweenedComplete2());
        }

        IEnumerator TweenedComplete2()
        {
            yield return new WaitForSeconds(1.4f);
            iTween.Stop(gameObject);                       
            transform.localScale = new Vector3(1, 1, 1);
            _callback?.Invoke();
            _callback = null;
        }
    }
}