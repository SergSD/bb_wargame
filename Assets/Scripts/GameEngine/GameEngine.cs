﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
namespace BB_WarGame
{
    public class GameEngine : MonoBehaviour
    {

        private PopupsManager _popupsManger;
        private CardsManager _cardsManager;


        void Start()
        {
            _cardsManager = GetComponent<CardsManager>();
            _popupsManger = GetComponent<PopupsManager>();
            SetupHandlers();
        }

        private void SetupHandlers()
        {            
            EventManager.StartListening(CustomGameEvent.GAME_FINISHED, OnGameFinished);           
            EventManager.StartListening(CustomGameEvent.START_NEW_GAME, OnRestartGame);           
        }

        private void RemoveHandlers()
        {
            EventManager.StopListening(CustomGameEvent.GAME_FINISHED, OnGameFinished);
            EventManager.StopListening(CustomGameEvent.START_NEW_GAME, OnRestartGame);
        }

        private void OnGameFinished(int? winnerId)
        {            
            _popupsManger.ShowEndGamePopup((int)winnerId);
        }

        private void OnRestartGame(int? roundNuber)
        {
            _cardsManager.SetupNewGame();
        }

        public void StartNewRound()
        {
            _cardsManager.StartTurn();
        }
        
        private void OnDestroy()
        {
            RemoveHandlers();
        }
    }
}