﻿using UnityEngine;
namespace ExtraUtils
{
    public static class Utils
    {
        public static void Shuffle<T>(T[] arr)
        {
            int n = arr.Length;
            System.Random rnd = new System.Random();
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                T value = arr[k];
                arr[k] = arr[n];
                arr[n] = value;
            }
        }

        public static void SwitchParent(GameObject obj, Transform targetParent)
        {
            Vector3 calulatedLocalPositon = targetParent.InverseTransformPoint(obj.transform.position);
            obj.transform.parent = targetParent;
            obj.transform.localPosition = calulatedLocalPositon;
        }

        //public static ConcatLists
    }
}