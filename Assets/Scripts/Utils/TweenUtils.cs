﻿using UnityEngine;
using System.Collections;

namespace ExtraUtils
{
    public static class TweenUtils
    {
        public static Vector2 GetQuadraticBezierCurvePosition(float t, Vector2 src, Vector2 dest, Vector2 control)
        {
            return new Vector2(
                 (1 - t) * (1 - t) * src.x + 2 * (1 - t) * t * control.x + t * t * dest.x,
                 (1 - t) * (1 - t) * src.y + 2 * (1 - t) * t * control.y + t * t * dest.y
                );
        }

        public static Vector2 GetLinearBezierCurvePosition(float t, Vector2 src, Vector2 dest)
        {
            return new Vector2(
                 (1-t)*src.x + t*dest.x,
                 (1 - t) * src.y + t * dest.y
                );
        }

        public static Vector2 GetCubicBezierCurvePosition(float t, Vector2 src, Vector2 dest, Vector2 control)
        {
            return new Vector2(
                 (1 - t) * (1 - t) * (1 - t) * src.x + 3 * (1 - t) * (1 - t) * t * control.x + 3 * (1 - t) * t * t * dest.x + t * t * t * dest.x,
                 (1 - t) * (1 - t) * (1 - t) * src.y + 3 * (1 - t) * (1 - t) * t * control.y + 3 * (1 - t) * t * t * dest.y + t * t * t * dest.y
                );
        }
    }
}