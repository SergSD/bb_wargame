﻿using UnityEngine;

namespace BB_WarGame
{
    public class CardSprites : MonoBehaviour
    {
        public const int DeckSize = 52;
        public Sprite InitSprite;
        [SerializeField] private Sprite[] _sprites = new Sprite[DeckSize];

        
        public Sprite GetSprite(int cardIdx)
        {
            return _sprites[cardIdx];
        }
    }
}