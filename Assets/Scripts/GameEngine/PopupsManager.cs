﻿using UnityEngine;
using System.Collections;
using System;
using BB_WarGame;

public class PopupsManager : MonoBehaviour
{
    [SerializeField] GameObject EndGamePopupPrefab;

    private GameObject _endGamePopup;

    // Use this for initialization
    void Start()
    {

    }

    public void ShowEndGamePopup(int playerId)
    {
        _endGamePopup = Instantiate(EndGamePopupPrefab);
        _endGamePopup.GetComponent<EndGamePopup>().Show(playerId==0, playerId, OnEndGamePopupCLosed);
    }

    private void OnEndGamePopupCLosed()
    {
        Destroy(_endGamePopup);
        EventManager.TriggerEvent(CustomGameEvent.START_NEW_GAME);
    }
}
