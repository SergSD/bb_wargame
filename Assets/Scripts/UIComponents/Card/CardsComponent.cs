﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ExtraUtils;
using System.Collections.Generic;
using System.Linq;
using System;
using TMPro;
using UnityEngine.Rendering;

namespace BB_WarGame
{
    public class CardsComponent : MonoBehaviour
    {
        private const int DECK_SIZE = 52;        
        [SerializeField] private CardSprites _sprites;
        [SerializeField] private GameObject _cardPrefab;
        [SerializeField] private SpriteRenderer _cardsDeck1;
        [SerializeField] private SpriteRenderer _cardsDeck2;
        [SerializeField] private Transform _meetingPointL;
        [SerializeField] private Transform _meetingPointR;
        [SerializeField] private Button[] _btns;

        private GameObject _playerCard;
        private GameObject _opponentCard;
        private GameObject _currentWinner;
        private GameObject _currentLooser;
        private List<GameObject> _currentTurnCardsHeap = new List<GameObject>();
        
        public TextMeshProUGUI playerCardsCount;
        public TextMeshProUGUI opponentCardsCount;

        public delegate void OnCompleteCallback(int numOfCards);
        private OnCompleteCallback _onCompleteCallback;


        // Use this for initialization
        void Start()
        {
      
        }
        public void PlayWarTurn(OnCompleteCallback callback)
        {
            _onCompleteCallback = callback;
            PreparePlayingCards(-1, -1);
            MoveCardsToCenter(FinishWarTurn);
        }

        public void PlayWarTurn(int playerCardInd, int opponentCardIndex, OnCompleteCallback callback)
        {
            _onCompleteCallback = callback;
            PreparePlayingCards(playerCardInd, opponentCardIndex);
            MoveCardsToCenter(FinishWarTurn);
        }

        public void PlayRegularTurn(int playerCardInd, int opponentCardIndex, int winnerId, OnCompleteCallback callback)
        {
            _onCompleteCallback = callback;            
            PreparePlayingCards(playerCardInd, opponentCardIndex);
            SetWinnerPlayer(winnerId);
            MoveCardsToCenter(EmphesiseWinnigCard);
        }

        private void PreparePlayingCards(int playerCardInd, int opponentCardIndex)
        {
            _playerCard = Instantiate(_cardPrefab);
            _playerCard.SetActive(true);
            _playerCard.GetComponent<SpriteRenderer>().sprite = playerCardInd > -1 ? _sprites.GetSprite(playerCardInd) : _sprites.InitSprite;
            _playerCard.transform.parent = _cardsDeck1.transform;
            _playerCard.transform.localPosition = new Vector3(0, 0, 0);

            _opponentCard = Instantiate(_cardPrefab);
            _opponentCard.SetActive(true);
            _opponentCard.GetComponent<SpriteRenderer>().sprite = opponentCardIndex > -1 ? _sprites.GetSprite(opponentCardIndex) : _sprites.InitSprite;
            _opponentCard.transform.parent = _cardsDeck2.transform;
            _opponentCard.transform.localPosition = new Vector3(0, 0, 0);

            _currentTurnCardsHeap.Add(_playerCard);
            _currentTurnCardsHeap.Add(_opponentCard);
        }

        private void SetWinnerPlayer(int winnerId)
        {
            if (winnerId == 1)
            {
                _currentWinner = _playerCard;
                _currentLooser = _opponentCard;
            }
            else if (winnerId == 2)
            {
                _currentWinner = _opponentCard;
                _currentLooser = _playerCard;
            }
        }

        private void MoveCardsToCenter(Action callback)
        {
            MoveCard mpc = _playerCard.GetComponent<MoveCard>();
            Vector3 localPositionR = _playerCard.transform.InverseTransformPoint(_meetingPointR.position);
            Vector2 targetPosR = new Vector2(localPositionR.x, localPositionR.y);
            Vector2 curveOffsetR = new Vector2(_playerCard.transform.localPosition.x + 0.5f, _playerCard.transform.localPosition.y + 0.5f);

            MoveCard moc = _opponentCard.GetComponent<MoveCard>();
            Vector3 localPositionL = _opponentCard.transform.InverseTransformPoint(_meetingPointL.position);
            Vector2 targetPosL = new Vector2(localPositionL.x, localPositionL.y);
            Vector2 curveOffsetL = new Vector2(_opponentCard.transform.localPosition.x - 2.0f, _opponentCard.transform.localPosition.y - 1.0f);

            mpc.AnimateMoveToDist(targetPosR, curveOffsetR, callback);
            moc.AnimateMoveToDist(targetPosL, curveOffsetL);
        }

        private void EmphesiseWinnigCard()
        {
            ScaleInOutCard wsc = _currentWinner.GetComponent<ScaleInOutCard>();
            FadeInOutCard lfc = _currentLooser.GetComponent<FadeInOutCard>();
            lfc.FadeCardOut(true, .2f);
            wsc.ScaleInOut(.2f, MoveCardsToWinnerDeck);
        }

        private void MoveCardsToWinnerDeck()
        {
            Transform winnersDeck;
            if (_currentWinner == _playerCard)
            {
                winnersDeck = _cardsDeck1.transform;
            }
            else
            {
                winnersDeck = _cardsDeck2.transform;
            }
            for (int i = 0; i < _currentTurnCardsHeap.Count; i++)
            {
                GameObject card = _currentTurnCardsHeap.ElementAt(i);
                Utils.SwitchParent(card, winnersDeck);
                Vector2 targetPos = new Vector2(0, 0);
                if (i == _currentTurnCardsHeap.Count - 1)
                {
                    card.GetComponent<MoveCard>().AnimateMoveToDist(targetPos, null, FinalizeTurn);
                }
                else
                    card.GetComponent<MoveCard>().AnimateMoveToDist(targetPos, null);
            }
        }

        private void FinishWarTurn()
        {
            _onCompleteCallback?.Invoke(0);
        }

        private void FinalizeTurn()
        {
            int numOfCards = _currentTurnCardsHeap.Count;
            foreach (GameObject card in _currentTurnCardsHeap) 
            {
                iTween.Stop(card);
                Destroy(card);
            }
            _currentTurnCardsHeap.Clear();
            _onCompleteCallback?.Invoke(numOfCards);
        }

        public void SetPlayerCardsLeft(int num)
        {
            playerCardsCount.text = num.ToString();
        }

        public void SetOpponentCardsLeft(int num)
        {
            opponentCardsCount.text = num.ToString();
        }

        public void SetButtonsActive(bool isActive)
        {
            foreach (Button btn in _btns)
            {
                btn.interactable = isActive;
            }
        }
    }
}